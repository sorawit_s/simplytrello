package com.example.tonystark.app.models;

import java.util.ArrayList;
import java.util.List;

public class Storage {

    private List<Note> savedNotes;

    public Storage()
    {
        savedNotes = new ArrayList<Note>();
    }

    public void saveNote(Note note) { savedNotes.add(note); }

    public int changeOrderNote(int oldNum,int oper)
    {
        int index = -99;
        List<Note> keep = new ArrayList<Note>();
        if(oldNum>=0 && oldNum< savedNotes.size())
        {
            if(oper==1&&oldNum<=savedNotes.size()-1) {
                keep.add(savedNotes.get(oldNum+1));
                savedNotes.set(oldNum + 1, savedNotes.get(oldNum));
                savedNotes.set(oldNum,keep.get(0));
                index = oldNum+1;
            }
            if(oper==-1&&oldNum>0) {
                keep.add(savedNotes.get(oldNum-1));
                savedNotes.set(oldNum-1,savedNotes.get(oldNum));
                savedNotes.set(oldNum,keep.get(0));
                index = oldNum-1;
            }
        }
        if(index==-99)
            return oldNum;
        return index;
    }

    public void deleteNote(Note note) {
        int index = 0;
        for(int i=0 ; i<savedNotes.size() ;i++)
        {
            if(savedNotes.get(i)==note)
                index = i;
        }
        savedNotes.remove(index);
    }

    public void deleteNote(int index) {
        savedNotes.remove(index);
    }

    public List<Note> loadNotes() {
        return savedNotes;
    }

}