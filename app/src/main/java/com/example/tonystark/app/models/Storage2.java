package com.example.tonystark.app.models;

import java.util.ArrayList;
import java.util.List;

public class Storage2 {

    private List<Card> savedNotes;

    private static Storage2 instance;

    private Storage2() {
        savedNotes = new ArrayList<Card>();
    }

    public static Storage2 getInstance() {
        if(instance == null) {
            instance = new Storage2();
        }
        return instance;
    }

    public void saveNote(Card card) {
        savedNotes.add(card);
        HandleStorage.getInstance().saveStorage(new Storage());
    }

    public int changeOrderNote(int oldNum,int oper)
    {
        int index = -99;
        List<Card> keep = new ArrayList<Card>();
        if(oldNum>=0 && oldNum< savedNotes.size())
        {
            if(oper==1&&oldNum<=savedNotes.size()-1) {
                keep.add(savedNotes.get(oldNum+1));
                savedNotes.set(oldNum + 1, savedNotes.get(oldNum));
                savedNotes.set(oldNum,keep.get(0));
                index = oldNum+1;
                HandleStorage.getInstance().changeOrderStorage(oldNum,1);
            }
            if(oper==-1&&oldNum>0) {
                keep.add(savedNotes.get(oldNum-1));
                savedNotes.set(oldNum - 1, savedNotes.get(oldNum));
                savedNotes.set(oldNum,keep.get(0));
                index = oldNum-1;
                HandleStorage.getInstance().changeOrderStorage(oldNum,-1);
            }
        }
        if(index==-99)
            return oldNum;
        return index;
    }

    public void deleteNote(Card card) {
        int index = 0;
        for(int i=0 ; i<savedNotes.size() ;i++)
        {
            if(savedNotes.get(i)==card)
                index = i;
        }
        savedNotes.remove(index);
        HandleStorage.getInstance().deleteStorage(index);
    }

    public void deleteNote(int index) {
        savedNotes.remove(index);
        HandleStorage.getInstance().deleteStorage(index);
    }

    public List<Card> loadNotes() {
        return savedNotes;
    }

}