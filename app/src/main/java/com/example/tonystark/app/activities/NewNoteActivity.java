package com.example.tonystark.app.activities;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.tonystark.app.R;
import com.example.tonystark.app.models.Note;
import com.example.tonystark.app.models.Storage;
import com.example.tonystark.app.models.HandleStorage;

public class NewNoteActivity extends AppCompatActivity {

    private TextView subject;
    private TextView body;
    private Button saveButton;

    private int storageIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_note);
        storageIndex = (Integer)getIntent().getSerializableExtra("storageIndex");
        initComponents();
    }

    private void initComponents() {
        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "angelina.TTF");
        subject = (TextView)findViewById(R.id.subject);
        subject.setTypeface(myTypeface);
        body = (TextView)findViewById(R.id.body);
        body.setTypeface(myTypeface);
        saveButton = (Button)findViewById(R.id.save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveNewNote();
                finish();
            }
        });
    }

    private void saveNewNote() {
        HandleStorage.getInstance().getStorage(storageIndex).saveNote(
                new Note(subject.getText().toString(),
                        body.getText().toString()));
    }
}
