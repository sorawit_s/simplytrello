package com.example.tonystark.app.models;

import java.io.Serializable;

public class Note implements Serializable {

    private String subject;
    private String body;

    public Note(String subject, String body) {
        this.subject = subject;
        this.body = body;
    }

    public String getSubject() { return subject; }
    public String getBody() { return body; }

    @Override
    public String toString() {
        return "Subject: " + subject + " Body: " + body;
    }
}