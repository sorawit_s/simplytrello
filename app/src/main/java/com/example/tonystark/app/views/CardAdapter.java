package com.example.tonystark.app.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.tonystark.app.R;
import com.example.tonystark.app.models.Card;

import java.util.List;

public class CardAdapter extends ArrayAdapter<Card> {

    public CardAdapter(Context context, int resource, List<Card> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if(v == null) {
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.card_cell, null);
        }

        TextView subject2 = (TextView)v.findViewById(R.id.subject2);
        TextView body2 = (TextView)v.findViewById(R.id.body2);

        Card card = getItem(position);
        subject2.setText(card.getSubject());
        body2.setText(card.getBody());

        return v;
    }
}
