package com.example.tonystark.app.models;

import java.util.ArrayList;
import java.util.List;

public class HandleStorage {

    private List<Storage> storage;

    private static HandleStorage instance;

    private HandleStorage() {
        storage = new ArrayList<Storage>();
    }

    public static HandleStorage getInstance() {
        if(instance == null) {
            instance = new HandleStorage();
        }
        return instance;
    }

    public void saveStorage(Storage newStorage) { storage.add(newStorage); }

    public void changeOrderStorage(int oldNum,int oper)
    {
        int index = -99;
        List<Storage> keep = new ArrayList<Storage>();
        if(oldNum>=0 && oldNum< storage.size())
        {
            if(oper==1&&oldNum<=storage.size()-1) {
                keep.add(storage.get(oldNum + 1));
                storage.set(oldNum + 1, storage.get(oldNum));
                storage.set(oldNum,keep.get(0));
                index = oldNum+1;
            }
            if(oper==-1&&oldNum>0) {
                keep.add(storage.get(oldNum-1));
                storage.set(oldNum-1,storage.get(oldNum));
                storage.set(oldNum,keep.get(0));
                index = oldNum-1;
            }
        }
    }

    public void deleteStorage(Storage deleteStorage) {
        int index = 0;
        for(int i=0 ; i<storage.size() ;i++)
        {
            if(storage.get(i)==deleteStorage)
                index = i;
        }
        storage.remove(index);
    }

    public void deleteStorage(int index) {
        storage.remove(index);
    }

    public Storage getStorage(int index)
    {
        return storage.get(index);
    }

    public List<Storage> loadStorage() {
        return storage;
    }

}