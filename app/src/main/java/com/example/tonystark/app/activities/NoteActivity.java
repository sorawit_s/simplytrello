package com.example.tonystark.app.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.tonystark.app.R;
import com.example.tonystark.app.models.Note;
import com.example.tonystark.app.models.Storage;

import java.util.List;

public class NoteActivity extends AppCompatActivity {

    private Note note;

    private TextView subject;
    private TextView body;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        note = (Note)getIntent().getSerializableExtra("note");
        setContentView(R.layout.activity_note);
        initComponents();
    }

    private void initComponents() {
        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "angelina.TTF");
        subject = (TextView)findViewById(R.id.subject);
        subject.setTypeface(myTypeface);
        body = (TextView)findViewById(R.id.body);
        body.setTypeface(myTypeface);
//        editButton = (Button)findViewById(R.id.edit_button);
//        editButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(NoteActivity.this, EditNoteActivity.class);
//                intent.putExtra("note", note);
//                startActivity(intent);
//            }
//      });
    }

    @Override
    protected void onStart() {
        super.onStart();
        subject.setText(note.getSubject());
        body.setText(note.getBody());
    }
}
