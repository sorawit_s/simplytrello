package com.example.tonystark.app.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.tonystark.app.R;
import com.example.tonystark.app.models.Note;

import java.util.List;

public class NoteAdapter extends ArrayAdapter<Note> {

    public NoteAdapter(Context context, int resource, List<Note> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if(v == null) {
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.note_cell, null);
        }

        TextView subject = (TextView)v.findViewById(R.id.subject);
        TextView body = (TextView)v.findViewById(R.id.body);

        Note note = getItem(position);
        subject.setText(note.getSubject());
        body.setText(note.getBody());

        return v;
    }
}
