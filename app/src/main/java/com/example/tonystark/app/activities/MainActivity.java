package com.example.tonystark.app.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.tonystark.app.R;
import com.example.tonystark.app.models.HandleStorage;
import com.example.tonystark.app.models.Note;
import com.example.tonystark.app.models.Storage;
import com.example.tonystark.app.models.Storage2;
import com.example.tonystark.app.views.NoteAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    //private TextView dummyNotes;
    private GridView notesGridView;
    private List<Note> notes;
    private NoteAdapter noteAdapter;

    private Button createButton;

    private EditText numCard;
    private Button deleteButton;
    private ImageButton upButton;
    private ImageButton downButton;

    private int storageIndex;

    private int selectGridView = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        storageIndex = (Integer)getIntent().getSerializableExtra("storageIndex");
        initComponents();
    }

    private void initComponents() {
        notes = new ArrayList<Note>();
        noteAdapter = new NoteAdapter(this, R.layout.note_cell, notes );
        notesGridView = (GridView)findViewById(R.id.notes_grid_view);
        notesGridView.setAdapter(noteAdapter);
        notesGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, NoteActivity.class);
                intent.putExtra("note", notes.get(i));
                startActivity(intent);
            }
        });
        createButton = (Button) findViewById(R.id.create_button);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewNoteActivity.class);
                intent.putExtra("storageIndex", storageIndex);
                startActivity(intent);
            }
        });
        numCard = (EditText)findViewById(R.id.num_note);
        deleteButton = (Button) findViewById(R.id.delete_note_button);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int index = Integer.parseInt(numCard.getText().toString());
                index = index-1;
                if(index>=0 && index < notes.size()) {
                    deleteNote(index);
                }
            }
        });
        upButton = (ImageButton) findViewById(R.id.up_button);
        upButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                System.out.println();
//                System.out.println("numCard = " + numCard.getText().toString());
                if (!numCard.getText().toString().equals(""))
                    orderCard(-1);
            }
        });
        downButton = (ImageButton) findViewById(R.id.down_button);
        downButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                System.out.println();
//                System.out.println("numCard = " + numCard.getText().toString());
                if (!numCard.getText().toString().equals(""))
                    orderCard(1);
            }
        });
        //dummyNotes = (TextView)findViewById(R.id.dummy_notes);
    }

    public void orderCard(int oper){
        int changeIndex = 0;
        int numIndex = -99;
        if(HandleStorage.getInstance().getStorage(storageIndex).loadNotes().size()>1)
        {
                if(!numCard.getText().toString().equals("")) {
                    numIndex = Integer.parseInt(numCard.getText().toString());
                    numIndex = numIndex - 1;
                }
//                System.out.println();
//                System.out.println("new numIndex = " + numIndex);
                if(numIndex>=0 && numIndex<( HandleStorage.getInstance().getStorage(storageIndex).loadNotes().size() ))
                {
                    changeIndex = HandleStorage.getInstance().getStorage(storageIndex).changeOrderNote(numIndex, oper);
                    numCard.setText(String.valueOf(changeIndex+1));
                    restart();
                }
//                if(changeIndex!=-99&&changeIndex!=q0)
//                numCard.setText(String.valueOf(changeIndex+1));
        }
    }

    private void deleteNote(int index) {
        HandleStorage.getInstance().getStorage(storageIndex).deleteNote(index);
        restart();
    }

    private void restart(){
        notes.clear();
        for(Note note: HandleStorage.getInstance().getStorage(storageIndex).loadNotes()) {
            notes.add(note);
        }
        noteAdapter.notifyDataSetChanged();
    }

    protected void onStart() {
        super.onStart();
        notes.clear();
        for(Note note: HandleStorage.getInstance().getStorage(storageIndex).loadNotes()) {
            notes.add(note);
        }

        noteAdapter.notifyDataSetChanged();
        //dummyNotes.setText(s);
    }
}