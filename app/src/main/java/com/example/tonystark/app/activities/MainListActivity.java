package com.example.tonystark.app.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;

import com.example.tonystark.app.R;
import com.example.tonystark.app.models.Card;
import com.example.tonystark.app.models.HandleStorage;
import com.example.tonystark.app.models.Storage;
import com.example.tonystark.app.models.Storage2;
import com.example.tonystark.app.views.CardAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainListActivity extends AppCompatActivity {

    //private TextView dummyNotes;
    private GridView cardsGridView;
    private List<Card> cards;
    private List<Storage> storages;
    private CardAdapter cardAdapter;

    private Button createButton;

    private EditText numCard;
    private ImageButton upButton;
    private ImageButton downButton;
    private Button deleteButton;


    private int selectGridView = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_list);
        initComponents();
    }

    private void initComponents() {
        cards = new ArrayList<Card>();
        storages = new ArrayList<Storage>();
        cardAdapter = new CardAdapter(this, R.layout.card_cell, cards );
        cardsGridView = (GridView)findViewById(R.id.cards_grid_view);
        cardsGridView.setAdapter(cardAdapter);
        cardsGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainListActivity.this, MainActivity.class);
                intent.putExtra("storageIndex", i);
                System.out.println();
                System.out.println("Storage = "+HandleStorage.getInstance().loadStorage().get(i).loadNotes().toString());
                startActivity(intent);
            }
        });
        createButton = (Button) findViewById(R.id.create_list_button);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainListActivity.this, NewCardActivity.class);
                startActivity(intent);
            }
        });
        numCard = (EditText)findViewById(R.id.num_list_card);
        deleteButton = (Button) findViewById(R.id.delete_card_button);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int index = Integer.parseInt(numCard.getText().toString());
                index = index-1;
                if(index>=0 && index < cards.size()) {
                    deleteNote(index);
                    deleteStorage(index);
                    restart();
                }
            }
        });
        upButton = (ImageButton) findViewById(R.id.up_list_button);
        upButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println();
                System.out.println("numCard = " + numCard.getText().toString());
                if (!numCard.getText().toString().equals("")) {
                    orderCard(-1);
                    restart();
                }
            }
        });
        downButton = (ImageButton) findViewById(R.id.down_list_button);
        downButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                System.out.println();
//                System.out.println("numCard = " + numCard.getText().toString());
                if (!numCard.getText().toString().equals("")) {
                    orderCard(1);
                    restart();
                }
            }
        });
        //dummyNotes = (TextView)findViewById(R.id.dummy_notes);
    }

    private void deleteNote(int index) {
        Storage2.getInstance().deleteNote(index);
    }

    private void deleteStorage(int index) {
        HandleStorage.getInstance().deleteStorage(index);
    }

    public void orderCard(int oper){
        int changeIndex = 0;
        int numIndex = -99;
        if(Storage2.getInstance().loadNotes().size()>1)
        {
                if(!numCard.getText().toString().equals("")) {
                    numIndex = Integer.parseInt(numCard.getText().toString());
                    numIndex = numIndex - 1;
                }
//                System.out.println();
//                System.out.println("new numIndex = " + numIndex);
                if(numIndex>=0 && numIndex<Storage2.getInstance().loadNotes().size()) {
                    changeIndex = Storage2.getInstance().changeOrderNote(numIndex, oper);
                    numCard.setText(String.valueOf(changeIndex+1));
                }
//                if(changeIndex!=-99&&changeIndex!=0)
//                numCard.setText(String.valueOf(changeIndex+1));
        }
    }

    private void restart(){
        cards.clear();
        for(Card card: Storage2.getInstance().loadNotes()) {
            cards.add(card);
        }
        storages.clear();
        for(Storage storage: HandleStorage.getInstance().loadStorage()) {
            storages.add(storage);
        }
        cardAdapter.notifyDataSetChanged();
    }

    protected void onStart() {
        super.onStart();
        cards.clear();
        for(Card card: Storage2.getInstance().loadNotes()) {
            cards.add(card);
        }
        storages.clear();
        for(Storage storage: HandleStorage.getInstance().loadStorage()) {
            storages.add(storage);
        }
        cardAdapter.notifyDataSetChanged();
        //dummyNotes.setText(s);
    }
}