package com.example.tonystark.app.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.tonystark.app.R;
import com.example.tonystark.app.models.Card;
import com.example.tonystark.app.models.Storage2;

public class CardActivity extends AppCompatActivity {

    private Card card;

    private TextView subject2;
    private TextView body2;
    private Button deleteButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        card = (Card)getIntent().getSerializableExtra("card");
        setContentView(R.layout.activity_card);
        initComponents();
    }

    private void initComponents() {
        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "angelina.TTF");
        subject2 = (TextView)findViewById(R.id.subject2);
        subject2.setTypeface(myTypeface);
        body2 = (TextView)findViewById(R.id.body2);
        body2.setTypeface(myTypeface);
//        editButton = (Button)findViewById(R.id.edit_button);
//        editButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(NoteActivity.this, EditNoteActivity.class);
//                intent.putExtra("note", note);
//                startActivity(intent);
//            }
//        });
        deleteButton = (Button)findViewById(R.id.delete_button2);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteNote(card);
                finish();
            }
        });
    }

    private void deleteNote(Card card) {
        Storage2.getInstance().deleteNote(card);
    }

    @Override
    protected void onStart() {
        super.onStart();
        subject2.setText(card.getSubject());
        body2.setText(card.getBody());
    }
}
