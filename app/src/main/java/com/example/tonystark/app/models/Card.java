package com.example.tonystark.app.models;

import java.io.Serializable;

public class Card implements Serializable {

    private String subject2;
    private String body2;

    public Card(String subject, String body) {
        this.subject2 = subject;
        this.body2 = body;
    }

    public String getSubject() { return subject2; }
    public String getBody() { return body2; }

    @Override
    public String toString() {
        return "Subject: " + subject2 + " Body: " + body2;
    }
}