package com.example.tonystark.app.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.tonystark.app.R;
import com.example.tonystark.app.models.Card;
import com.example.tonystark.app.models.HandleStorage;
import com.example.tonystark.app.models.Storage;
import com.example.tonystark.app.models.Storage2;

public class NewCardActivity extends AppCompatActivity {

    private TextView subject2;
    private TextView body2;
    private Button saveButton2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_card);
        initComponents();
    }

    private void initComponents() {
        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "angelina.TTF");
        subject2 = (TextView)findViewById(R.id.subject2);
        subject2.setTypeface(myTypeface);
        body2 = (TextView)findViewById(R.id.body2);
        body2.setTypeface(myTypeface);
        saveButton2 = (Button)findViewById(R.id.save_button2);
        saveButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveNewNote();
                finish();
            }
        });
    }

    private void saveNewNote() {
        Storage2.getInstance().saveNote(
                new Card(subject2.getText().toString(),
                        body2.getText().toString()));
        HandleStorage.getInstance().saveStorage(new Storage());
    }
}
